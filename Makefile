postgres:
	docker run -d --name shrink-db -p 5432:5432 -e POSTGRES_USER=shrink -e POSTGRES_PASSWORD=shrink -e POSTGRES_DB=shrink postgres:11-alpine
