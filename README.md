# This is Shrink UI

[Shrink](https://shrink.n-th.me) is a project for shortening url. It is made with CSS, HTML, JavaScript and love.

#

#### Requirements
 - npm v6.14.7
 - node v12.18.0
 - serverless v1.73.1
 - Docker v19.03.8
 - docker-compose v1.25.0
 - AWS CLI v2

#

## How to run

***This is a tutorial for Linux users.***

After cloning this project, go to your root folder and install the dependencies using your package manager for JavaScript.

```
npm install
```

### Local server
If you'd like to run Serverless server locally, you will need a docker for Postgres. Please, use this command:
```
make postgres
```

If you don't have this command for Linux, it is necessary to install
```
sudo apt-get install build-essential
```

To start serverless

```
sls offline start
```
#

## How to deploy

### To deploy our Lambda


```
sls deploy
```

### AWS deploy

After installing aws cli, and setting aws configuration we need to create our stack with cloudformation's files.

```
aws cloudformation create-stack --stack-name shrink-frontend --template-body file://.aws/root.template.yaml --region sa-east-1  --profile n-th
```

To upload files in S3, run

```
aws s3 sync public s3://shrink.n-th.me --profile n-th
```

***Note that the configuration present in Cloudformation are for Shrink project!***

#

## Cool stuff
Here are some technologies used in this project:

- NestJs
- AWS (Lambda + API Gateway + CloudFormation + CodeBuild + CloudWatch)
- TypeORM
- Postgres as a Service

#

Why Nest? Why Serverless? Check our documentation [here](https://www.notion.so/b2c3ead850d249858de3989683be623c?v=58d3396e28a3421b97db073906a67ec7).


Also, check [Shrink UI repository](https://gitlab.com/nathsg/shrinkui).
