import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn
} from "typeorm";

@Entity("url")
export class URLEntity {
  @PrimaryGeneratedColumn("uuid") id: string;

  @CreateDateColumn() created: Date;

  @Column("text") short_uri: string;

  @Column("text") long_url: string;

  @Column("integer") count: number;
}
