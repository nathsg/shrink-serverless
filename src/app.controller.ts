import {
  Controller,
  Get,
  Param,
  Header,
  Logger,
  HttpStatus
} from "@nestjs/common";
import { Redirect } from "@nestjsplus/redirect";
import { URLService } from "./url/url.service";

@Controller()
export class AppController {
  constructor(private readonly urlService: URLService) {}

  @Get("app/:shortUri")
  @Redirect()
  @Header("Access-Control-Allow-Origin", "*")
  async getRedirect(@Param("shortUri") shortUri: string) {
    const urlData = await this.urlService.getUrl(shortUri);

    if (Object.keys(urlData).length) {
      const longUrl: string = urlData[0].long_url;
      return { statusCode: HttpStatus.TEMPORARY_REDIRECT, url: longUrl };
    }

    return { statusCode: HttpStatus.NOT_FOUND };
  }
}
