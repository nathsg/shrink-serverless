import { Module } from "@nestjs/common";
import { APP_FILTER } from "@nestjs/core";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ConfigModule } from "nestjs-config";
import * as path from "path";

import { AppController } from "./app.controller";
import { TypeOrmConfigService } from "./config/typeorm.config";
import { HttpErrorFilter } from "./shared/http-error.filter";
import { URLModule } from "./url/url.module";

@Module({
  imports: [
    ConfigModule.load(
      path.resolve(__dirname, "config", "**", "!(*.d).{ts,js}")
    ),
    TypeOrmModule.forRootAsync({
      inject: [ConfigModule],
      useClass: TypeOrmConfigService
    }),
    URLModule
  ],
  controllers: [AppController],
  providers: [
    {
      provide: APP_FILTER,
      useClass: HttpErrorFilter
    }
  ]
})
export class AppModule {}
