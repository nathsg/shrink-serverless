import { Injectable } from "@nestjs/common";
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from "@nestjs/typeorm";
import { ConnectionManager, getConnectionManager } from "typeorm";

@Injectable()
export class TypeOrmConfigService implements TypeOrmOptionsFactory {
  async createTypeOrmOptions(): Promise<TypeOrmModuleOptions> {
    const connectionManager: ConnectionManager = getConnectionManager();
    let options: any;

    if (connectionManager.has("default")) {
      options = connectionManager.get("default").options;
      await connectionManager.get("default").close();
    } else {
      options = {
        type: "postgres",
        host: process.env.SQL_HOST,
        username: process.env.SQL_USER,
        password: process.env.SQL_PASSWORD,
        database: process.env.SQL_DATABASE,
        port: parseInt(process.env.SQL_PORT, 10),
        entities: [__dirname + "/../entity/**.entity{.ts,.js}"],
        synchronize: true,
        logging: true
      } as TypeOrmModuleOptions;
    }
    return options;
  }
}
