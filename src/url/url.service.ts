import { Injectable, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import * as base32 from "base32";

import { URLEntity } from "../entity/url.entity";
import { URLDTO } from "./url.dto";

@Injectable()
export class URLService {
  constructor(
    @InjectRepository(URLEntity)
    private urlRepository: Repository<URLEntity>
  ) {}

  async createUrl(body: URLDTO): Promise<URLEntity> {
    const longUrl = body.longUrl;

    const queryUri = `SELECT * FROM "url" WHERE long_url = '${longUrl}'`;
    const urlEntity = await this.urlRepository.query(queryUri);

    if (urlEntity.length) {
      return await this.urlRepository.findOne(urlEntity[0].id);
    }

    const encoded: string = base32.encode(longUrl);
    const newURL: URLEntity = new URLEntity();

    newURL.short_uri = encoded.slice(0, 7);
    newURL.long_url = longUrl;
    newURL.count = 0;

    return await this.urlRepository.save(newURL);
  }

  async get(): Promise<URLEntity[]> {
    const query = `SELECT * FROM "url" ORDER BY count DESC LIMIT 5`;
    return await this.urlRepository.query(query);
  }

  async getUrl(shortUri: string): Promise<URLEntity> {
    const queryUri = `SELECT * FROM "url" WHERE short_uri = '${shortUri}'`;
    const urlEntity = await this.urlRepository.query(queryUri);

    if (urlEntity.length) {
      this.updateCount(shortUri);
    }

    return await this.urlRepository.query(queryUri);
  }

  private async updateCount(shortUri: string): Promise<URLEntity> {
    const queryCount = `UPDATE "url" SET count = count + 1 WHERE short_uri = '${shortUri}'`;
    return await this.urlRepository.query(queryCount);
  }
}
