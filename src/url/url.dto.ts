import { IsString, IsNotEmpty } from "class-validator";

export class URLDTO {
  @IsString()
  @IsNotEmpty()
  longUrl: string;
}
