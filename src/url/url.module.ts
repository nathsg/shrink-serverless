import { TypeOrmModule } from "@nestjs/typeorm";
import { Module } from "@nestjs/common";

import { URLEntity } from "../entity/url.entity";
import { URLController } from "./url.controller";
import { URLService } from "./url.service";

@Module({
  imports: [TypeOrmModule.forFeature([URLEntity]), URLEntity],
  controllers: [URLController],
  providers: [URLService],
  exports: [URLService]
})
export class URLModule {}
