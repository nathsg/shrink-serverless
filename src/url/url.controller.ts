import { Controller, Get, Body, Post, Param, Header } from "@nestjs/common";

import { URLDTO } from "./url.dto";
import { URLService } from "./url.service";
import { URLEntity } from "../entity/url.entity";

@Controller("url")
export class URLController {
  constructor(private readonly urlService: URLService) {}

  @Get()
  @Header("Access-Control-Allow-Origin", "*")
  async getAll(): Promise<URLEntity[]> {
    return this.urlService.get();
  }

  @Post()
  @Header("Access-Control-Allow-Origin", "*")
  async createUrl(@Body() longUrl: URLDTO): Promise<URLEntity> {
    return this.urlService.createUrl(longUrl);
  }

  @Get(":shortUri")
  @Header("Access-Control-Allow-Origin", "*")
  async getUrl(@Param("shortUri") shortUri: string): Promise<URLEntity> {
    return this.urlService.getUrl(shortUri);
  }
}
